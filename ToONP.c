/*
 * Author: Tomasz Kępa
 * email: tk359746@students.mimuw.edu.pl
 */
#include "ToONP.h"

int main(int argc, char **argv)
{
  /* pipe descriptors */
  int from_child[2], to_child[2];
  char pipe_to_parent_dsc_str[10];
  char pipe_from_parent_dsc_str[10];
  /* buffer strings */
  char *to_process;
  char *result;
  char stack[] = "";
  char current_result[] = "";
  /* buffer counts */
  int to_process_len;
  int result_len;
  int stack_len = 1;
  int current_result_len = 1;

  /* Checking argument validity */
  if (argc != 2) {
    fatal("ToONP: Usage: expects arithmetic expression as an argument\n");
  }

  /* Reading input */
  to_process = argv[1];
  to_process_len = strlen(to_process) + 1;

  /* Creating pipes */
  if (pipe(from_child) == -1) {
    syserr("ToONP: Error in pipe(from_child)\n");
  }
  if (pipe(to_child) == -1) {
    syserr("ToONP: Error in pipe(to_child)\n");
  }

  switch (fork()) {
    case -1:
      syserr("ToONP: Error in fork()\n");

    case 0: /* child process */

      /* Closing usused descriptors */
      if (close(from_child[0]) == -1) {
        syserr("ToONP: Error in close(from_child[0])\n");
      }
      if (close(to_child[1]) == -1) {
        syserr("ToONP: Error in close(to_child[1])\n");
      }

      /* Converting file descriptors to strings */
      sprintf(pipe_to_parent_dsc_str, "%d", from_child[1]);
      sprintf(pipe_from_parent_dsc_str, "%d", to_child[0]);

      /* Executing first working process */
      execlp("./w", "w", pipe_from_parent_dsc_str, pipe_to_parent_dsc_str, (char *) 0);
      syserr("ToONP: Error in execlp\n");

    default: /* parent process */

      /* Closing usused descriptors */
      if (close(from_child[1]) == -1) {
        syserr("ToONP: Error in close(from_child[1])\n");
      }
      if (close(to_child[0]) == -1) {
        syserr("ToONP: Error in close(to_child[0])\n");
      }

      /* Sending data to first working process */
      write_to_pipe_with_length_and_error_message(to_child[1], &to_process_len,
         to_process, "ToONP: Error in write(to_child[1])\n");
      write_to_pipe_with_length_and_error_message(to_child[1], &stack_len,
         stack, "ToONP: Error in write(to_child[1])\n");
      write_to_pipe_with_length_and_error_message(to_child[1], &current_result_len,
         current_result, "ToONP: Error in write(to_child[1])\n");

      /* Closing unused descriptor */
      if (close(to_child[1]) == -1) {
        syserr("ToONP: Error in close(to_child[1])\n");
      }

      /* Receiving the result */
      read_from_pipe_with_length_and_error_message(from_child[0], &result_len,
        0, &result, "ToONP: Error in read(from_child[0])");

      /* Printing the result */
      printf("%s\n", result);

      /* Free usused resources */
      free(result);

      /* Closing unused descriptor */
      if (close(from_child[0]) == -1) {
        syserr("ToONP: Error in close(from_child[0])\n");
      }

      /* Waiting for child to close, collecting return code */
      if (wait(0) == -1) {
        syserr("ToONP: Error in wait\n");
      }

      return 0;
  }
}
