#!/bin/sh
# argumentem jest nazwa pliku do rozsubmitowania

if [ $# -ne 1 ]; then
  echo unsubmit: Uzywaj unsubmit plik-z-listem 
elif [ ! -f $1 -o ! -r $1 ]; then
  echo $0: nie moge odczytac $1
else
  nazwa=`grep ^begin $1|cut -f3 -d" "`
  student=`echo $nazwa|cut -f1 -d-`
  if mkdir $student; then # jesli katalog istnieje, to nie niszczymy zawartosci
    mv $1 $student
    cd $student
    uudecode $1
    files=`tar xvf $nazwa | grep -v /\$`
    rm $nazwa $1
    for p in $files ; do
      if ! mv $p .; then
        exit 1
      fi
    done 
    for k in *; do
      if [ -d $k ] ; then
        rm -r $k
      fi
    done
  fi
fi

