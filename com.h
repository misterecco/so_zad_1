#ifndef _COM_
#define _COM_

extern void write_to_pipe_with_length_and_error_message(
    int pipe_dsc, int *data_lenth_ptr, char* data_ptr, char* error_msg);

extern void read_from_pipe_with_length_and_error_message(
    int pipe_dsc, int *data_lenth_ptr, int buffer, char** data_ptr, char* error_msg);

#endif
