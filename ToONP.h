#ifndef _TOONP_
#define _TOONP_

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <limits.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include "err.h"
#include "com.h"

#define MAX_TOKEN_SIZE 100

#endif
