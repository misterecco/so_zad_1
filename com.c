/*
 * Author: Tomasz Kępa
 * email: tk359746@students.mimuw.edu.pl
 */
#include "ToONP.h"

void write_to_pipe_with_length_and_error_message(
    int pipe_dsc, int *data_lenth_ptr, char* data_ptr, char* error_msg)
{
  if (write(pipe_dsc, data_lenth_ptr, sizeof(int)) == -1) {
    syserr(error_msg);
  }
  if (write(pipe_dsc, data_ptr, *data_lenth_ptr) == -1) {
    syserr(error_msg);
  }
}

void read_from_pipe_with_length_and_error_message(
    int pipe_dsc, int *data_lenth_ptr, int buffer, char** data_ptr, char* error_msg)
{
  if (read(pipe_dsc, data_lenth_ptr, sizeof(int)) == -1) {
    syserr(error_msg);
  }
  if ((*data_ptr = malloc(*data_lenth_ptr * sizeof(char) + buffer)) == NULL) {
    fatal("Error in malloc()\n");
  }
  if (read(pipe_dsc, *data_ptr, *data_lenth_ptr) == -1) {
    syserr(error_msg);
  }
}
