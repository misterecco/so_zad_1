/*
 * Author: Tomasz Kępa
 * email: tk359746@students.mimuw.edu.pl
 */
#include "ToONP.h"

/* Local variables */

/* buffer strings */
char *to_process;
char *result;
char *stack;
char *current_result;
char token[MAX_TOKEN_SIZE];
/* buffer counts */
int to_process_len;
int result_len;
int stack_len;
int current_result_len;
int token_len;
int char_processed = 0;

char from_stack = '\0';

/* Local functions */

/* Functions operating on stack */

void stack_push(char val)
{
  stack[stack_len++] = val;
}

bool stack_is_empty()
{
  return stack_len == 1;
}

char stack_pop()
{
  if (stack_is_empty()) {
    fatal("w: Error in stack_pop(): stack empty\n");
  }
  return stack[--stack_len];
}

char stack_peek()
{
  if (stack_is_empty()) {
    fatal("w: Error in stack_peek(): stack empty\n");
  }
  return stack[stack_len-1];
}

/* Functions operating on result string */

void result_add_string(char* token)
{
  if (current_result_len != 1) {
    current_result[current_result_len - 1] = ' ';
    current_result[current_result_len++] = '\0';
  }
  strcat(current_result, token);
  current_result_len += token_len;
}

void result_add_char(char op)
{
  if (current_result_len != 1) {
    current_result[current_result_len - 1] = ' ';
    current_result[current_result_len++] = '\0';
  }
  current_result[current_result_len - 1] = op;
  current_result[current_result_len++] = '\0';
}

/* Main converting algorithm helper functions */

/* Function checking whether first argument
 * has higher or equal priority than the second */
bool operator_ge(char left_op, char right_op)
{
  switch(left_op) {

    case '^': /* Pursuant to FAQ, treating ^ operator as left-associative */
      if (right_op == '^') {
        return true;
      }
      return false;

    case '*':
    case '/':
      if (right_op == '^' || right_op == '*' || right_op == '/') {
        return true;
      }
      return false;

    case '-':
    case '+':
      if (right_op == '^' || right_op == '/' || right_op == '*'
          || right_op == '-' || right_op == '+') {
        return true;
      }
      return false;

    default:
      return false;
  }
}

/* Checks whether there's nothing else to process */
bool processed_expression_ended()
{
  return strcmp(to_process + char_processed, "") == 0;
}

/* Process one token of the expression */
void make_one_step()
{
  int whitespace_count = 0;
  while (isspace(to_process[whitespace_count])) {
    whitespace_count++;
  }
  char_processed = whitespace_count;

  if (sscanf(to_process, "%s", token) != EOF) {
    token_len = strlen(token);

    switch(token[0]) {
      case '^':
      case '*':
      case '/':
      case '+':
      case '-':
        while (!stack_is_empty() && operator_ge(token[0], stack_peek())) {
          result_add_char(stack_pop());
        }
        stack_push(token[0]);
        char_processed += 1;
        break;

      case '(':
        stack_push(token[0]);
        char_processed += 1;
        break;

      case ')':
        while ((from_stack = stack_pop()) != '(') {
          result_add_char(from_stack);
        }
        char_processed += 1;
        break;

      default:
        result_add_string(token);
        char_processed += token_len;
        break;
    }
  }
  if (processed_expression_ended()){
    while (!stack_is_empty()) {
      result_add_char(stack_pop());
    }
  }

  to_process_len -= char_processed;
}

int main(int argc, char **argv)
{
  /* pipe descriptors */
  int from_child[2], to_child[2];
  int from_parent, to_parent;
  char pipe_to_parent_dsc_str[10];
  char pipe_from_parent_dsc_str[10];

  /* Cheking arguments validity */
  if (argc != 3) {
    fatal("w: Usage: expects two pipe descriptors as arguments\n");
  }

  from_parent = atoi(argv[1]);
  to_parent = atoi(argv[2]);

  /* Reading data from previous process */
  read_from_pipe_with_length_and_error_message(from_parent, &to_process_len,
      0, &to_process, "w: Error in read(from_parent)\n");
  read_from_pipe_with_length_and_error_message(from_parent, &stack_len,
      1, &stack, "w: Error in read(from_parent)\n");
  read_from_pipe_with_length_and_error_message(from_parent, &current_result_len,
      (stack_len * 2 + MAX_TOKEN_SIZE + 2), &current_result, "w: Error in read(from_parent)\n");

  /* Closing unused descriptor */
  if (close(from_parent) == -1) {
    syserr("w: Error in close(from_parent)\n");
  }

  make_one_step();

  /* Sending end result back to parent */
  if (processed_expression_ended()) {
    write_to_pipe_with_length_and_error_message(to_parent, &current_result_len,
        current_result, "w: Error in write(to_parent)\n");
    return 0;
  }

  /* Creating pipes */
  if (pipe(from_child) == -1) {
    syserr("w: Error in pipe(from_child)\n");
  }
  if (pipe(to_child) == -1) {
    syserr("w: Error in pipe(to_child)\n");
  }

  switch (fork()) {
    case -1:
      syserr("w: Error in fork()\n");

    case 0: /* child process */

      /* Closing usused descriptors */
      if (close(from_child[0]) == -1) {
        syserr("w: Error in close(from_child[0])\n");
      }
      if (close(to_child[1]) == -1) {
        syserr("w: Error in close(to_child[1])\n");
      }
      if (close(to_parent) == -1) {
        syserr("w: Error in close(to_parent)\n");
      }

      /* Converting file descriptors to strings */
      sprintf(pipe_to_parent_dsc_str, "%d", from_child[1]);
      sprintf(pipe_from_parent_dsc_str, "%d", to_child[0]);

      /* Executing next working process */
      execlp("./w", "w", pipe_from_parent_dsc_str, pipe_to_parent_dsc_str, (char *) 0);
      syserr("w: Error in execlp\n");

    default: /* parent process */

      /* Closing usused descriptors */
      if (close(from_child[1]) == -1) {
        syserr("w: Error in close(from_child[1])\n");
      }
      if (close(to_child[0]) == -1) {
        syserr("w: Error in close(to_child[0])\n");
      }

      /* Sending data to next working process */
      write_to_pipe_with_length_and_error_message(to_child[1], &to_process_len,
          to_process + char_processed, "w: Error in write(to_child[1])\n");
      write_to_pipe_with_length_and_error_message(to_child[1], &stack_len,
          stack, "w: Error in write(to_child[1])\n");
      write_to_pipe_with_length_and_error_message(to_child[1], &current_result_len,
          current_result, "w: Error in write(to_child[1])\n");

      /* Free unused resources */
      free(to_process);
      free(stack);
      free(current_result);

      /* Close unused descriptors */
      if (close (to_child[1]) == -1) {
        syserr("w: Error in close(to_child[1])\n");
      }

      /* Recieving the result */
      read_from_pipe_with_length_and_error_message(from_child[0], &result_len,
          0, &result, "w: Error in read\n");

      /* Push the result recieved from child back to parent */
      write_to_pipe_with_length_and_error_message(to_parent, &result_len, result,
          "w: Error in write(to_parent)\n");

      /* Free unused resources */
      free(result);

      /* Close unused descriptors */
      if (close (from_child[0]) == -1) {
        syserr("w: Error in close(from_child[0])\n");
      }
      if (close (to_parent) == -1) {
        syserr("w: Error in close(to_parent)\n");
      }

      /* Waiting for child to close, collecting return code */
      if (wait(0) == -1) {
        syserr("w: Error in wait\n");
      }

      return 0;
  }

  return 0;


}
